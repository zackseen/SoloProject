-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 12 Okt 2022 pada 04.29
-- Versi server: 10.4.24-MariaDB
-- Versi PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbspp`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_transaksi`
--

CREATE TABLE `detail_transaksi` (
  `id_detail` int(1) NOT NULL,
  `id_transaksi` int(1) NOT NULL,
  `id_jenis_pembayaran` int(1) NOT NULL,
  `bulan_dibayar` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_pembayaran`
--

CREATE TABLE `jenis_pembayaran` (
  `id_jenis_pembayaran` int(1) NOT NULL,
  `nama_transaksi` varchar(20) NOT NULL,
  `nominal` int(10) NOT NULL,
  `tahun_ajaran` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `jenis_pembayaran`
--

INSERT INTO `jenis_pembayaran` (`id_jenis_pembayaran`, `nama_transaksi`, `nominal`, `tahun_ajaran`) VALUES
(1, 'SPP ( 1 bulan )', 175000, '2021/2022'),
(3, 'Daftar Ulang', 517000, '2021/2022');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbpetugas`
--

CREATE TABLE `tbpetugas` (
  `id_petugas` int(1) NOT NULL,
  `nama_petugas` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` text NOT NULL,
  `no_hp` varchar(17) NOT NULL,
  `jabatan` enum('KEPALA SEKOLAH','WALI KELAS','TELLER','') NOT NULL,
  `hak_akses` enum('ADMIN','KASIR') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbpetugas`
--

INSERT INTO `tbpetugas` (`id_petugas`, `nama_petugas`, `username`, `password`, `no_hp`, `jabatan`, `hak_akses`) VALUES
(6, 'Alvito Dwinova', 'AL', '$2y$10$ITkClgf120L8e9fyIc0u.O.N.fLV0S4HgKHPgSGqXpCrLIBoN36Hq', '082345678', 'KEPALA SEKOLAH', 'ADMIN');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbsiswa`
--

CREATE TABLE `tbsiswa` (
  `id_siswa` int(1) NOT NULL,
  `nama_siswa` varchar(100) NOT NULL,
  `nis` int(15) NOT NULL,
  `kelas` enum('XII RPL 1','XII RPL 2','XII MM 1','XII MM 2','XII AKL 1','XII AKL 2','XII AKL 3','XII AKL 4','XII AKL 5','XII OTKP 1','XII OTKP 2','XII OTKP 3','XII BDP 1','XII BDP 2','XII BDP 3') NOT NULL,
  `tahun_masuk` varchar(10) NOT NULL,
  `no_rek` int(20) NOT NULL,
  `jk` enum('L','P') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbsiswa`
--

INSERT INTO `tbsiswa` (`id_siswa`, `nama_siswa`, `nis`, `kelas`, `tahun_masuk`, `no_rek`, `jk`) VALUES
(1, 'Alvito Dwinova', 2147483647, 'XII RPL 1', '2020', 1235617, 'L'),
(3, 'Andi febrianto', 2147483647, 'XII RPL 1', '2020', 12345, 'L');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbtransaksi`
--

CREATE TABLE `tbtransaksi` (
  `id_transaksi` int(1) NOT NULL,
  `id_siswa` int(1) NOT NULL,
  `id_petugas` int(1) NOT NULL,
  `tanggal_bayar` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbtransaksi`
--

INSERT INTO `tbtransaksi` (`id_transaksi`, `id_siswa`, `id_petugas`, `tanggal_bayar`) VALUES
(1, 1, 6, '2022-10-12');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD PRIMARY KEY (`id_detail`);

--
-- Indeks untuk tabel `jenis_pembayaran`
--
ALTER TABLE `jenis_pembayaran`
  ADD PRIMARY KEY (`id_jenis_pembayaran`);

--
-- Indeks untuk tabel `tbpetugas`
--
ALTER TABLE `tbpetugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- Indeks untuk tabel `tbsiswa`
--
ALTER TABLE `tbsiswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- Indeks untuk tabel `tbtransaksi`
--
ALTER TABLE `tbtransaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  MODIFY `id_detail` int(1) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `jenis_pembayaran`
--
ALTER TABLE `jenis_pembayaran`
  MODIFY `id_jenis_pembayaran` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tbpetugas`
--
ALTER TABLE `tbpetugas`
  MODIFY `id_petugas` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `tbsiswa`
--
ALTER TABLE `tbsiswa`
  MODIFY `id_siswa` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tbtransaksi`
--
ALTER TABLE `tbtransaksi`
  MODIFY `id_transaksi` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
